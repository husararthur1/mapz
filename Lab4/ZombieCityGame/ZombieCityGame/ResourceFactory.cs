﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieCityGame.Builder;

namespace ZombieCityGame
{
    public interface IResource
    {
        Resource Produce();
    }

    public class Resource : IResource
    {
        private ResourceType type;

        private int durability;
        public Resource(ResourceType type)
        {
            durability = 0;
            this.type = type;
        }
        public string GetType()
        {
            return this.type.GetResourceType();
        }
        public Resource Produce()
        {
            durability += 10;
            return this;
        }
    }

    public class ResourceType
    {
        private string type;
        private Building manufacturer;
        public ResourceType(string type, Building manufacturer)
        {
            this.type = type;
            this.manufacturer = manufacturer;
        }
        public string GetResourceType()
        {
            return this.type;
        }
    }

    class ResourceTypeFactory
    {
        private static Dictionary<string, ResourceType> resourceTypes = new Dictionary<string, ResourceType>();

        public static ResourceType GetType(string type, Building manufactory)
        {
            if (!resourceTypes.ContainsKey(type))
            {
                resourceTypes[type] = new(type, manufactory);
            }
            return resourceTypes[type];
        }
    }
}
