﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieCityGame
{
    public interface IDecorator
    {
        void Attack();
        void TakeCover();
    }

    public class Decorator : IZombieAttacker
    {
        protected IZombieAttacker wrapped;
        public Decorator(IZombieAttacker attacker)
        {
            this.wrapped = attacker;
        }
        public ZombieState State { get; set; }
        public double Speed { get => wrapped.Speed; set => wrapped.Speed = value; }
        public double HP { get => wrapped.HP; set => wrapped.HP = value; }
        public double Damage { get => wrapped.Damage; set => wrapped.Damage = value; }

        public void Attack()
        {
            wrapped.Attack();
        }

        public void Move()
        {
            wrapped.Move();
        }

        public void TakeCover()
        {
            wrapped.TakeCover();
        }
    }
    public class FireZombie : Decorator
    {
        public FireZombie(IZombieAttacker zombie) : base(zombie)
        {
            wrapped.State = new AggresiveAttacker(zombie);
        }
        public void Attack()
        {
            Burn();
            base.Attack();
        }

        public void TakeCover()
        {
            Burn();
            base.TakeCover();
        }
        private void Burn()
        {
            wrapped.HP -= 4;
            wrapped.Damage += 0.5;
        }
    }
    public class PoisonZombie : Decorator
    {
        public PoisonZombie(IZombieAttacker zombie) : base(zombie)
        {
            wrapped.State = new AggresiveAttacker(zombie);
        }
        public void Attack()
        {
            Poison();
            base.Attack();
        }

        public void TakeCover()
        {
            base.TakeCover();
        }
        private void Poison()
        {
            if (wrapped.HP < 100)
            {
                wrapped.HP += 4;
            }
            wrapped.Damage *= 0.8;
        }
    }
}
