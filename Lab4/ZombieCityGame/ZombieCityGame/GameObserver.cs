﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieCityGame.AbstractFactory;
using ZombieCityGame.Builder;

namespace ZombieCityGame
{
    public interface IGameObserver
    {
        void Attach(IUnitObserver observer);
        void Detach(IUnitObserver observer);
        void Notify();
    }
    public interface IUnitObserver
    {
        void Update(double unitDamage);
        double Attack();
    }
    public class GameObserver : IGameObserver
    {
        private List<IUnitObserver> unitObservers = new List<IUnitObserver>();
        private double _waveDamage;
        private double _towerDamage;
        public void Attach(IUnitObserver observer)
        {
            unitObservers.Add(observer);
        }
        public void Detach(IUnitObserver observer)
        {
            unitObservers.Remove(observer);
        }
        public void Notify()
        {
            foreach (var unitObserver in unitObservers)
            {
                double damage = unitObserver.Attack();
                unitObserver.Update(damage);
            }
        }
    }
    public class WaveObserver : IUnitObserver
    {
        private IZombieWave _wave;
        private double totalDamage;
        public WaveObserver(IZombieWave wave)
        {
            _wave = wave;
        }
        public void Update(double towerDamage)
        {
            _wave.ReceiveDamage(towerDamage);
        }
        public double Attack()
        {
            return _wave.Attackers.Sum(attacker => attacker.Damage);
        }
        public IZombieWave GetWave() => _wave;
    }

    public class BuildingObserver : IUnitObserver
    {
        private List<TowerBuilder> towers;
        
        public BuildingObserver(List<TowerBuilder> towers)
        {
            this.towers = towers;
        }
        public  void Update(double zombieDamage)
        {
            double damage = zombieDamage / towers.Count();
            foreach (var tower in towers) 
            {
                tower.ReceiveDamage(damage);
            }
        }
        public double Attack()
        {
            return towers.Sum(tower => tower.Defend());
        }
        public List<Building> GetTowers()
        {
            List<Building> towerList = new List<Building>();
            foreach(var tower in towers) {
                towerList.Add(tower.GetBuilding());
            }
            return towerList;
        }
    }
}
