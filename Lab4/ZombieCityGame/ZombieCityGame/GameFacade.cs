﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ZombieCityGame.GameManager;
using System.Windows.Controls;
using ZombieCityGame.AbstractFactory;

namespace ZombieCityGame
{
    public class GameFacade
    {
        public Level level { get; private set; }
        //private IZombieWave wave;
        private Canvas GameScreen;
        private ComboBox Tiles;
        private double cityAreaWidth;
        private void CreateMap(CityMapUI map)
        {
            var all = GameScreen.Children.OfType<Image>().ToList();
            var images = GameScreen.Children.OfType<Image>()
                .Where(image => image.Source.ToString() ==
                "pack://application:,,,/images/city-tile.png").ToList();

            foreach (var image in images)
            {
                map.InsertMapCell(image, new Position()
                { x = Canvas.GetLeft(image), y = Canvas.GetTop(image) });
            }
            for (int i = 0; i < images.Count; i++)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = $"{i}";
                Tiles.Items.Add(item);
            }
        }

        public void StartGame(CityMapUI map)
        {
            CreateMap(map);
            UpdateLevel();
            //CreateWave();
        }
        public void UpdateLevel(int waveCount = 0)
        {
            if (waveCount % 2 == 1)
            {
                level = Level.Advanced;
            }
            else
            {
                level = Level.Casual;
            }
        }
        public void Setlevel(Level _level)
        {
            level = _level;
        }
        //public void CreateWave()
        //{
        //    if (level.Equals(Level.Casual))
        //    {
        //        wave.WaveSize = 4;
        //        wave = new FirstZombieWave();
        //    }
        //    else if (level.Equals(Level.Advanced))
        //    {
        //        wave = new SecondZombieWave();
        //        wave.WaveSize = 10;
        //    }
        //}
        public void FireUpAttackers(List<IZombieAttacker> attackers)
        {
            for (int i = 0; i < attackers.Count; i++)
            {
                FireZombie fireZombie = new FireZombie(attackers[i]);
                attackers[i] = fireZombie;
            }
        }
        public void PoisonAttackers(List<IZombieAttacker> attackers)
        {
            for (int i = 0; i < attackers.Count; i++)
            {
                PoisonZombie poisonZombie = new PoisonZombie(attackers[i]);
                attackers[i] = poisonZombie;
            }
        }
        public void UpdateAttackers(List<ZombieAttackerUI> zombies)
        {
            foreach (var zombie in zombies)
            {
                if (zombie.Zombie is IZombieAttacker)
                {
                    string newImagePath = "";
                    if (zombie.Zombie is FireZombie)
                    {
                        newImagePath = "pack://application:,,,/ZombieCityGame;component/images/zombie-fireattacker.png";
                        zombie.SetImage(newImagePath);

                    }
                    else if (zombie.Zombie is PoisonZombie)
                    {
                        newImagePath = ("pack://application:,,,/ZombieCityGame;component/images/zombie-poisonedattacker.png");
                        zombie.SetImage(newImagePath);
                    }
                }
            }
        }
        public GameFacade(Canvas screen, ComboBox tiles, double cityArea)
        {
            GameScreen = screen;
            Tiles = tiles;
            cityAreaWidth = cityArea;
            //wave = new FirstZombieWave();
        }
    }
}
