﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace ZombieCityGame
{
    public abstract class ZombieState 
    {
        protected IZombieAttacker zombie;// = new ZombieAttacker();
        public IZombieAttacker Zombie
        {
            get { return zombie; }
            set { zombie = value; }
        }
        public abstract void UpdateHP(double hp);
        public abstract void Interact();
    }

    public class DefensiveAttacker : ZombieState 
    {
        public DefensiveAttacker(ZombieState state) 
        {
            this.zombie = state.Zombie;
        }
        public DefensiveAttacker(ZombieAttacker zombie)
        {
            this.zombie = zombie;
        }
        public override void UpdateHP(double hp)
        {
            if(hp < 30)
            {
                zombie.State = new PassiveAttacker(this);
            } 
            else if (hp > 70) 
            {
                zombie.State = new AggresiveAttacker(this);
            } 
            else if (hp <= 0)
            {
                zombie.State = new Dead(this);
            }
            this.zombie.HP = hp;
        }
        public override void Interact()
        {
            zombie.Damage -= 0.1;
            zombie.Speed -= 0.5;
            zombie.TakeCover();
            zombie.Attack();
        }
    }
    public class AggresiveAttacker : ZombieState
    {
        public AggresiveAttacker(ZombieState state)
        {
            this.zombie = state.Zombie;
        }
        public AggresiveAttacker(IZombieAttacker zombie)
        {
            this.zombie = zombie;
        }
        public override void UpdateHP(double hp)
        {
            if (hp < 30)
            {
                zombie.State = new PassiveAttacker(this);
            }
            else if (hp > 30 && hp < 70)
            {
                zombie.State = new DefensiveAttacker(this);
            }
            else if (hp <= 0)
            {
                zombie.State = new Dead(this);
            }
            this.zombie.HP = hp;
        }
        public override void Interact()
        {
            zombie.Damage += 0.1;
            zombie.Attack();
        }
    }
    public class PassiveAttacker: ZombieState
    {
        public PassiveAttacker(ZombieState state)
        {
            this.zombie = state.Zombie;
        }
        public PassiveAttacker(ZombieAttacker zombie)
        {
            this.zombie = zombie;
        }
        public override void UpdateHP(double hp)
        {
            if (hp > 70)
            {
                zombie.State = new AggresiveAttacker(this);
            }
            else if (hp > 30 && hp < 70)
            {
                zombie.State = new DefensiveAttacker(this);
            }
            else if (hp <= 0)
            {
                zombie.State = new Dead(this);
            }
            this.zombie.HP = hp;
        }
        public override void Interact()
        {
            zombie.Damage += 0;
            zombie.Speed = 0;
            zombie.TakeCover();
        }
    }
    public class Dead: ZombieState
    {
        public Dead(ZombieState state)
        {
            this.zombie = state.Zombie;
        }
        public override void UpdateHP(double hp)
        {
            hp = 0;
            this.zombie.HP = 0;
        }
        public override void Interact()
        {
            zombie.Damage = 0;
            zombie.Speed = 0;
        }
    }
}
