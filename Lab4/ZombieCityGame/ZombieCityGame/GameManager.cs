﻿using System.Drawing;
using System.Reflection.Emit;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using ZombieCityGame.AbstractFactory;
using ZombieCityGame.Builder;
using ZombieCityGame.Singleton;
using static ZombieCityGame.GameManager;


namespace ZombieCityGame
{
    public class GameManager
    {
        public enum Level
        {
            Casual = 0,
            Advanced = 1,
        }
        public enum BuildingOption
        {
            TownHall = 0,
            Workshop,
            Tower
        }
        public enum ZombieType
        {
            Dummy = 0,
            Attacker
        }
        public enum AttackerType
        {
            Attacker = 0,
            AdvancedAttacker,
            Poisoned
        }

        public class ZombieDummyUI
        {
            private IZombieDummy zombie;
            private Position position;
            public Position Position { get => position; }


            public Image ZombieImage { get; }
            public void Move()
            {
                zombie.Move();
                Position position = Position;
                

                position.x -= position.x * zombie.Speed;
                position.y = position.y;
                SetPosition(position);
                if(position.x < 600)
                {
                    zombie.Speed = 0;
                    return;
                }
                Canvas.SetLeft(ZombieImage, position.x);
                Canvas.SetTop(ZombieImage, position.y);
            }
            public void SetImage(string imagePath)
            {
                ZombieImage.Source = new BitmapImage(new Uri(imagePath, UriKind.RelativeOrAbsolute));
            }
            public void SetPosition(Position pos)
            {
                position = pos;
            }
            public void Spawn()
            {
                Canvas.SetLeft(ZombieImage, Position.x);
                Canvas.SetTop(ZombieImage, Position.y);
            }
            public void Stand()
            {
                Zombie.Stand();
            }
            public ZombieDummyUI(IZombieDummy zombie)
            {
                this.zombie = zombie;
                position = new Position();
                ZombieImage = new Image();
            }
            public void Update()
            {
                if (zombie.HP == 0)
                {
                    SetImage("");
                }
            }
            public IZombieDummy Zombie { get => zombie; set => zombie = value; }
        }

        public class ZombieAttackerUI
        {
            private IZombieAttacker zombie;
            private Position position;
            public Position Position { get => position; }
            public double Damage { get; set; }
            public Image ZombieImage { get; }
            public void Move()
            {
                if(zombie.State is Dead)
                {
                    return;
                }
                zombie.Move();
                Position position = Position;


                position.x -= position.x * zombie.Speed;
                position.y = position.y;
                SetPosition(position);
                if (position.x < 600)
                {
                    zombie.Speed = 0;
                    return;
                }
                Canvas.SetLeft(ZombieImage, position.x);
                Canvas.SetTop(ZombieImage, position.y);
            }
            public void SetImage(string imagePath)
            {
                ZombieImage.Source = new BitmapImage(new Uri(imagePath, UriKind.RelativeOrAbsolute));
            }
            public void SetPosition(Position pos)
            {
                position = pos;
            }
            public void Spawn()
            {
                Canvas.SetLeft(ZombieImage, Position.x);
                Canvas.SetTop(ZombieImage, Position.y);
            }
            public void Attack()
            {
                Zombie.Attack();
            }
            public void Update()
            {
                if (zombie.State is Dead)
                {
                    SetImage("pack://application:,,,/ZombieCityGame;component/images/tombstone.png");
                    zombie.Speed = 0;
                } 
                else
                {
                    zombie.State.Interact();
                }
            }
            public ZombieAttackerUI(IZombieAttacker zombie)
            {
                this.zombie = zombie;
                position = new Position();
                ZombieImage = new Image();
            }
            public IZombieAttacker Zombie { get => zombie; set => zombie = value; }
        }

        public class BuildingUI
        {
            public Position position { get; set; }
            public Image BuildingImage { get; set; }

            public BuildingUI()
            {
                position = new Position();
                BuildingImage = new Image();
            }
        }
        interface IBuildingBuilderUI
        {
            void UpdatePosition(Position Position);
            void SetImage(string imagePath);
            void SpawnBuilding();

            //void UpdateHP(float HP);
        }
        public class BuildingBuilderUI: IBuildingBuilderUI
        {
            private IBuildingBuilder builder;
            private BuildingUI building;
            private string imagePath;
            public BuildingBuilderUI(IBuildingBuilder builder, string imagePath)
            {
                this.builder = builder;
                this.building = new BuildingUI();
                this.imagePath = imagePath;
            }

            public IBuildingBuilder GetBuilder()
            {
                return builder;
            }
            public void SetBuilder(IBuildingBuilder builder)
            {
                this.builder = builder;
            }
            public BuildingUI GetBuildingUI()
            {
                return building;
            }
            public void UpdatePosition(Position Position)
            {
                building.position = Position;
            }
            public void SetImage(string imagePath)
            {
                building.BuildingImage.Source = new BitmapImage(new Uri(imagePath, UriKind.RelativeOrAbsolute));
            }
            public void SpawnBuilding()
            {
                building.BuildingImage.Height = 70;
                building.BuildingImage.Width = 66;
                Canvas.SetLeft(building.BuildingImage, building.position.x);
                Canvas.SetTop(building.BuildingImage, building.position.y);
            }
            public BuildingUI ConstructBuilding()
            {
                SetImage(imagePath);
                SpawnBuilding();

                return building;
            }
        }
        public class CellClickedEventArgs : EventArgs
        {
            public MapCell ClickedCell { get; }

            public CellClickedEventArgs(MapCell clickedCell)
            {
                ClickedCell = clickedCell;
            }
        }
        public class MapCell
        {
            private Position position { get; set; }
            private Image image { get; set; }
            public Position Position => position;
            public BuildingUI Building { get; set; }
            public Image Image => image;
            public MapCell? SelectedCell { get; set; }
            public MapCell()
            {
                position = new Position();
                image = new Image();
                image.MouseDown += Image_MouseDown;
                Building = new BuildingUI();
            }

            public MapCell(Position position, Image image)
            {
                this.position = position;
                this.image = image;
                image.MouseDown += Image_MouseDown;
                Building = new BuildingUI();
            }

            public MapCell(Position position, Image image, BuildingBuilderUI builder)
            {
                this.position = position;
                this.image = image;
                image.MouseDown += Image_MouseDown;
                this.Building = builder.GetBuildingUI();
            }

            public event EventHandler<CellClickedEventArgs>? CellClicked;
            private void Image_MouseDown(object sender, MouseButtonEventArgs e)
            {
                CellClicked?.Invoke(this, new CellClickedEventArgs(this));
            }
            public void PlaceBuilding(BuildingBuilderUI builder)
            {
                Position position = new Position();
                //builder.ConstructBuilding();
                
                this.Building = builder.ConstructBuilding();
                position.x = this.Position.x + Building.BuildingImage.Width / 3;
                position.y = this.Position.y + Building.BuildingImage.Height / 5;
                builder.UpdatePosition(position);
                builder.SpawnBuilding();
            }
        }
        public class CityMapUI
        {
            private City city = City.GetInstance();
            public City City { get => City.GetInstance(); }
            private List<MapCell> cityMap = new List<MapCell>();
            private List<BuildingUI> buildings = new List<BuildingUI>();
            public MapCell SelectedCell = new MapCell();
            public List<MapCell> CityMap { get => cityMap; set => cityMap = value; }
            public List<BuildingUI> Buildings
            {
                get => buildings;
            }

            public void InsertMapCell(Image mapCell, Position cellCoords)
            {
                var cell = new MapCell(cellCoords, mapCell);
                cell.CellClicked += Cell_Clicked;
                cityMap.Add(cell);
            }

            private void Cell_Clicked(object sender, CellClickedEventArgs e)
            {
                SelectedCell = e.ClickedCell;
            }
        }
    }
}
