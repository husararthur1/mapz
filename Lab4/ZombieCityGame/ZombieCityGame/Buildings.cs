﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace ZombieCityGame
{
    namespace Builder
    {
        //Builder
        public class Building
        {
            public double HP { get; set; }
            List<Resource> resources;
            public List<Resource> Resources { get => resources; set => resources = value; }
            public Building()
            {
                HP = 100;
                resources = new List<Resource>();    
            }
            public void ReceiveDamage(double damage) 
            {
                if(HP < damage)
                {
                    HP = 0;
                } 
                else
                {
                    HP -= damage;
                }
            }
        }
        public interface IBuildingBuilder
        {
            void UpdateHP(double HP);
            void UpdateResources(List<Resource> resources);
            void ReceiveDamage(double Damage);
            Building GetBuilding();
        }

        public interface ITower
        {
            double Defend();
            void ReceiveResource(object sender, ResourceEventArgs e);
        }
        public interface IWorkShop
        {
            Resource Craft(string resourceType);
            void Customize();
        }
        public interface ITownHall
        {
            //Task<List<IResource>> SendResourcesToTower(ITower tower);
            //Task RequestResources(IWorkShop workShop);
            void ReceiveResource(object sender, ResourceEventArgs e);
            void RequestResource(string resource, object sender);
        }
        public class ResourceEventArgs : EventArgs
        {
            public Resource Resource { get; set; }

            public ResourceEventArgs(string resource)
            {
                ResourceType type = ResourceTypeFactory.GetType(resource, new Building());
                Resource = new Resource(type);
            }
        }
        public delegate void ResourceRequestHandler(object sender, ResourceEventArgs e);

        public class TowerBuilder : ResourceHandler, IBuildingBuilder, ITower
        {
            readonly Building tower = new Building();
            public void UpdateHP(double HP)
            {
                tower.HP = HP;
            }
            public void ReceiveResource(object sender, ResourceEventArgs e)
            {
                tower.Resources.Add(e.Resource);
            }
            public void UpdateResources(List<Resource> resources)
            {
                for(int i = 0; i < resources.Count; i++)
                {
                    tower.Resources.Add(resources[i]);
                }
            }
            public void ReceiveDamage(double damage)
            {
                tower.HP -= damage;
            }
            public double Defend()
            {
                double damage = 1.0 + tower.HP % 100;
                return damage;
            }
            public override List<Resource> SendResources(string resource, int amount)
            {
                if (tower.Resources.Count != amount || 
                    tower.Resources.Any(towerResource => towerResource.GetType() != resource))
                {
                    return base.SendResources(resource, amount);
                }
                return tower.Resources;
            }
            public Building GetBuilding()
            {
                return tower;
            }
        }
        public class WorkShopBuilder : IBuildingBuilder, IWorkShop
        {
            Building workshop = new Building();
            public void UpdateResources(List<Resource> resources)
            {
                for (int i = 0; i < resources.Count; i++)
                {
                    workshop.Resources.Add(resources[i]);
                }
            }
            public void UpdateHP(double HP)
            {
                workshop.HP = HP;
            }
            public void ReceiveDamage(double damage)
            {
                workshop.HP -= damage;
            }
            public Resource Craft(string resourceType)
            {
                ResourceType type = ResourceTypeFactory.GetType(resourceType, workshop);
                Resource resource = new Resource(type);
                return resource.Produce();
            }
            
            public void Customize()
            {

            }
            public Building GetBuilding()
            {
                return workshop;
            }
            public void ProvideResource(object sender, ResourceEventArgs resE)
            {
                TownHallBuilder tb = new TownHallBuilder();
                tb.ReceiveResource(this, resE);
            }
        }
        
        public class TownHallBuilder : IBuildingBuilder, ITownHall
        {
            Building townHall = new Building();
            private List<IResource> receivedResources = new List<IResource>();
            public event ResourceRequestHandler? ResourceRequested;
            public void RequestResource(string resource, object sender)
            {
                Console.WriteLine("TownHall receiving request for resource: " + resource);
                // Raise the event
                ResourceRequested?.Invoke(sender, new ResourceEventArgs(resource));
            }
            public void ReceiveResource(object sender, ResourceEventArgs e)
            {
                townHall.Resources.Add(e.Resource);

            }
            public void ProvideResource(object sender, ResourceEventArgs e)
            {
                if(townHall.Resources.Contains(e.Resource))
                {
                    townHall.Resources.Remove(e.Resource);
                } else
                {
                    RequestResource(e.Resource.GetType(), sender);
                } 
            }
            public void UpdateHP(double HP)
            {
                townHall.HP = HP;
            }
            public void UpdateResources(List<Resource> resources)
            {
                for (int i = 0; i < resources.Count; i++)
                {
                    townHall.Resources.Add(resources[i]);
                }
            }
            public void ReceiveDamage(double damage)
            {
                townHall.HP -= damage;
            }
            public Building GetBuilding()
            {
                return townHall;
            }
        }
    }
}