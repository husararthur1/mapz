﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieCityGame.Builder;

namespace ZombieCityGame
{
    public interface IWorkshopDecorator
    {
        Resource Craft(string resource);
        void Customize();
    }

    public class WorkshopDecorator : IWorkshopDecorator
    {
        protected IWorkShop wrapped;
        public WorkshopDecorator(IWorkShop wrapped)
        {
            this.wrapped = wrapped;
        }

        public Resource Craft(string resource)
        {
            return wrapped.Craft(resource);
        }
        //public Task<List<IResource>> SendFix(ITownHall townHall)
        //{
        //    return wrapped.SendFix(townHall);
        //}
        public void Customize()
        {
            wrapped.Customize();
        }
    }
    public class LumberMill : WorkshopDecorator
    {
        WorkShopBuilder builder = new WorkShopBuilder();
        public LumberMill(IWorkShop workshop) : base(workshop)
        {

        }
        
        //public Task<List<IResource>> SendFix(ITownHall townHall)
        //{
        //    return base.SendFix(townHall);
        //}
        public void Customize()
        {
            base.Customize();
        }
        public Resource ProduceWood()
        {
            return base.Craft("wood");
            //ResourceType woodType = ResourceTypeFactory.GetType("wood", builder.GetBuilding());
            //Resource wood = new Resource(woodType);              
            //return wood;
        }
    }

    public class CoalMine : WorkshopDecorator
    {
        WorkShopBuilder builder = new WorkShopBuilder();
        public CoalMine(IWorkShop workshop) : base(workshop)
        {

        }
        public void Customize()
        {
            base.Customize();
        }
        public Resource ProduceCoal()
        {
            return base.Craft("coal");
        }
    }
    public class BlackSmithShop : WorkshopDecorator
    {
        WorkShopBuilder builder = new WorkShopBuilder();
        public BlackSmithShop(IWorkShop workshop) : base(workshop)
        {

        }
        public void Customize ()
        {
            base.Customize();
        }
        public Resource ProduceSteel()
        {
            return base.Craft("steel");
        }
    }
    public interface IResourceHandler
    {
        IResourceHandler SetNext(IResourceHandler handler);
        List<Resource> SendResources(string resoure, int amount);
    }
    public abstract class ResourceHandler : IResourceHandler
    {
        private IResourceHandler? _resourceHandler;
        

        public IResourceHandler SetNext(IResourceHandler handler)
        {
            _resourceHandler = handler;

            return _resourceHandler;
        }
        public virtual List<Resource> SendResources(string resoure, int amount)
        {
            if (_resourceHandler != null)
            {
                return _resourceHandler.SendResources(resoure, amount);
            }
            else
            {
                return null;
            }
        }
    }
    public class LumberMillRH : ResourceHandler
    {
        private LumberMill mill;
        public LumberMillRH(LumberMill mill)
        {
            this.mill = mill;
        }
        public LumberMill GetLumberMill() => this.mill;
        public override List<Resource> SendResources(string resoure, int amount)
        {
            if(resoure == "wood")
            {
                List <Resource> wood = new List <Resource>();
                for (int i = 0; i < amount; i++)
                {
                    wood.Add(mill.ProduceWood());
                }
                return wood;
            }
            else
            {
                return base.SendResources(resoure, amount);
            }
        }
    }
    public class BlackSmithRH : ResourceHandler
    {
        private BlackSmithShop smithShop;
        public BlackSmithRH(BlackSmithShop SmithShop)
        {
            this.smithShop = SmithShop;
        }
        public override List<Resource> SendResources(string resoure, int amount)
        {
            if (resoure == "steel")
            {
                List<Resource> metal = new List<Resource>();
                for (int i = 0; i < amount; i++)
                {
                    metal.Add(smithShop.ProduceSteel());
                }
                return metal;
            }
            else
            {
                return base.SendResources(resoure, amount);
            }
        }
    }
    public class CoalMineRH : ResourceHandler
    {
        private CoalMine coalMine;
        public CoalMineRH(CoalMine CoalMine) 
        {
            this.coalMine = CoalMine;
        }
        public override List<Resource> SendResources(string resoure, int amount)
        {
            if (resoure == "coal")
            {
                List<Resource> coal = new List<Resource>();
                for (int i = 0; i < amount; i++)
                {
                    coal.Add(coalMine.ProduceCoal());
                }
                return coal;
            }
            else
            {
                return base.SendResources(resoure, amount);
            }
        }
    }
}
