﻿using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZombieCityGame.AbstractFactory;
using ZombieCityGame.Singleton;
using ZombieCityGame.Builder;
using System.Windows.Media.Media3D;
using static ZombieCityGame.GameManager;

namespace ZombieCityGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GameFacade gameFacade;
        private DispatcherTimer GameTimer = new DispatcherTimer();
        private DispatcherTimer NightTimer = new DispatcherTimer();
        private CityMapUI map;
        private bool isNight = false, isStarted = false;
        //private List<ZombieUI> zombies = new List<ZombieUI>();
        private List<ZombieDummyUI> ZombieDummies = new List<ZombieDummyUI>();
        private List<ZombieAttackerUI> ZombieAttackers = new List<ZombieAttackerUI>();
        private List<WorkShopBuilder> ws = new List<WorkShopBuilder>();
        private int hours = 18, mins = 0, waveCount = 0;
        
        private List<TowerBuilder> towers = new List<TowerBuilder> { };
        private List<BuildingBuilderUI> workshops = new List<BuildingBuilderUI>();
        private List<Resource> wood = new List<Resource>();
        private List<Resource> steel = new List<Resource>();
        private List<Resource> coal = new List<Resource>();
        private WaveContext waveContext = new WaveContext(new DummiesStormStrategy());
        void UpdateHP(double hp, Image unit)
        {
            if (hp < 60)
            {
                unit.Source = new BitmapImage(
                        new Uri("pack://application:,,,/ZombieCityGame;component/images/mid-HP.png", UriKind.Relative));
            }
            else if (hp < 25)
            {
                unit.Source = new BitmapImage(
                        new Uri("pack://application:,,,/ZombieCityGame;component/images/low-HP.png", UriKind.Relative));
            }
            else
            {
                unit.Source = new BitmapImage(
                        new Uri("pack://application:,,,/ZombieCityGame;component/images/full-HP.png", UriKind.Relative));
            }
        }
        private void UpdateWavesHP()
        {
            foreach (var dummy in ZombieDummies)
            {
                UpdateHP(dummy.Zombie.HP, dummy.ZombieImage);
            }
            foreach (var attacker in ZombieAttackers)
            {
                UpdateHP(attacker.Zombie.HP, attacker.ZombieImage);
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            foreach (var c in map.CityMap)
            {
                CityLog.Text += $" x: {c.Position.x} y: {c.Position.y} height: {c.Image.ActualHeight} width: {c.Image.ActualWidth}\n";
            }
        }

        private void CreateBuilding_Click(object sender, RoutedEventArgs e)
        {
            if (map.CityMap.Count == map.Buildings.Count)
            {
                return;
            }

            if (tiles.SelectedItem is null)
            {
                return;
            }
            IBuildingBuilder builder;
            string imagePath = "";
            switch (buildings.SelectedIndex)
            {
                case (int)BuildingOption.TownHall:
                    builder = new TownHallBuilder();
                    imagePath = "pack://application:,,,/ZombieCityGame;component/images/townhall.png";
                    break;
                case (int)BuildingOption.Workshop:
                    builder = new WorkShopBuilder();
                    imagePath = "pack://application:,,,/ZombieCityGame;component/images/workshop.png";
                    break;
                case (int)BuildingOption.Tower:
                    builder = new TowerBuilder();
                    imagePath = "pack://application:,,,/ZombieCityGame;component/images/tower.png";
                    break;
                default:
                    return;
            }
            BuildingBuilderUI builderUI = new BuildingBuilderUI(builder, imagePath);
            builderUI.ConstructBuilding();
            if (builder is TownHallBuilder townHallBuilder)
            {
                if (map.City.TownHall != null) return;
                map.City.TownHall = townHallBuilder.GetBuilding();
            }
            else if (builder is TowerBuilder tower)
            {
                towers.Add(tower);
            }
            else if (builder is WorkShopBuilder workShop)
            {
                ws.Add(workShop);
                workshops.Add(builderUI);
                ComboBoxItem item = new ComboBoxItem();
                item.Content = $"Workshop {ws.Count()}";
                WorkshopsList.Items.Add(item);
            }

            map.Buildings.Add(builderUI.GetBuildingUI());

            map.SelectedCell = map.CityMap[tiles.SelectedIndex];


            map.SelectedCell.PlaceBuilding(builderUI);

            var image = map.SelectedCell.Building.BuildingImage;
            Canvas.SetZIndex(image, 1);
            GameScreen.Children.Add(image);

            tiles.Items.Remove(tiles.SelectedItem);
            tiles.Items.Refresh();
            map.CityMap.Remove(map.SelectedCell);
        }
        
        private void UpgradeWorkShop_Click(object sender, RoutedEventArgs e)
        {
            var workshop = workshops[WorkshopsList.SelectedIndex];

            if (NewWorkShops.SelectedIndex == 0)
            {
                workshop.SetImage("pack://application:,,,/ZombieCityGame;component/images/lumber-mill.png");
                workshop.SpawnBuilding();

            }
            else if (NewWorkShops.SelectedIndex == 1)
            {
                workshop.SetImage("pack://application:,,,/ZombieCityGame;component/images/smithshop.png");
                workshop.SpawnBuilding();

            }
            else if (NewWorkShops.SelectedIndex == 2)
            {
                workshop.SetImage("pack://application:,,,/ZombieCityGame;component/images/coalmine.png");
                workshop.SpawnBuilding();
            }
            else
            {
                return;
            }
        }
        

        private List<Resource> RequestResourceChain(string resource, int amount, IWorkShop workShop)
        {
            LumberMill mill = new LumberMill(workShop);
            BlackSmithShop bsShop = new BlackSmithShop(workShop);
            CoalMine coalMine = new CoalMine(workShop);

            LumberMillRH lumberMillHandler = new LumberMillRH(mill);
            BlackSmithRH blackSmithHandler = new BlackSmithRH(bsShop);
            CoalMineRH coalMineRH = new CoalMineRH(coalMine);

            lumberMillHandler.SetNext(blackSmithHandler).SetNext(coalMineRH);

            return lumberMillHandler.SendResources(resource, amount);
        }
        public MainWindow()
        {
            InitializeComponent();
            GameScreen.Focus();
            GameTimer.Interval = TimeSpan.FromMicroseconds(16);
            GameTimer.Tick += GameTick;
            GameTimer.Start();

            map = new CityMapUI();
            gameFacade = new GameFacade(GameScreen, tiles, CityArea.ActualWidth);
            gameFacade.StartGame(map);
            
            
            NightTimer.Interval = TimeSpan.FromSeconds(1);
            NightTimer.Tick += NightTick;
            NightTimer.Start(); 
            buildings.SelectedItem = buildings.Items[0];
        }
        private void NightTick(object? sender, EventArgs e)
        {
            if(mins >= 60)
            {
                hours++;
                mins = 0;
            }
            if(hours == 24)
            {
                hours = 0;
            }
            
            Timer.Content = $"Time: {hours.ToString("00")}:{mins.ToString("00")}";
            
            if (hours == 0 && mins == 0)
            {
                waveCount++;
                isNight = true;
                ZombieArea.Fill = Brushes.DarkBlue;
                var dummies = new DummiesStormStrategy();
                dummies.SetWave(new FirstZombieWave(waveCount*2));
                waveContext.SetStrategy(dummies);
                waveContext.Spawn();
                //SpawnZombies(waveContext.GetZombieWave());
                SpawnDummies(waveContext.GetZombieWave());
                
            } 
            else if(hours == 3 && mins == 0)
            {
                //gameFacade.PoisonAttackers(wave.Attackers);
                var attackers = new AttackersDawnStrategy();
                attackers.SetWave(new SecondZombieWave(waveCount));
                waveContext.SetStrategy(attackers);
                waveContext.Spawn();
                SpawnAttackers(waveContext.GetZombieWave());

                attackers.SetWave(new FirstZombieWave(waveCount));
                waveContext.SetStrategy(attackers);
                waveContext.Spawn();
                SpawnAttackers(waveContext.GetZombieWave());

                //UpdateZombieUI(ZombieAttackers, waveContext.Attackers);
                gameFacade.UpdateAttackers(ZombieAttackers);
            }
            else if(hours == 5 && mins == 0)
            {
                var lateNightZombies = new LateNightStrategy();
                lateNightZombies.SetWave(new SecondZombieWave(waveCount * 2));
                waveContext.SetStrategy(lateNightZombies);
                waveContext.Spawn();
                SpawnDummies(waveContext.GetZombieWave());
                SpawnAttackers(waveContext.GetZombieWave());
            } 
            else if (hours == 7 && mins == 0)
            {
                foreach (var attacker in ZombieAttackers)
                {
                    if (attacker.Zombie is ZombieAttacker _attacker)
                    {
                        _attacker.State = new Dead(_attacker.State);
                    }
                }

                foreach (var attacker in waveContext.Attackers)
                {
                    if(attacker is ZombieAttacker _attacker)
                    {
                        _attacker.State = new Dead(_attacker.State);
                    }
                }
            }
            else if (hours == 8 && mins == 0)
            {
                ZombieArea.Fill = Brushes.Bisque;
                gameFacade.FireUpAttackers(waveContext.Attackers);
                UpdateZombieUI(ZombieAttackers, waveContext.Attackers);
                gameFacade.UpdateAttackers(ZombieAttackers);
                isStarted = false;
            }
            if(isStarted)
            {
                mins += 10;
                return;
            }
            mins += 30;
        }
        void UpdateZombieUI(List<ZombieAttackerUI> zombieUI, List<IZombieAttacker> attackers)
        {
            int j = 0; 
            foreach(var zombie in zombieUI)
            {
                if (zombie.Zombie is PoisonZombie)
                {
                    zombie.Zombie = attackers[j];
                    j++;
                }
            }
        }
        private Position GenaratePosition(Canvas canvas, double x, double height)
        {
            Random random = new Random();

            double randomY = random.NextDouble() * canvas.ActualHeight / 2 + height / 2;

            return new Position { x = x, y = randomY };
        }

        private void btnRequest_Wood_Click(object sender, RoutedEventArgs e)
        {
            if (ws.Count == 0)
            {
                return;
            }
            List<Resource> newWood = RequestResourceChain("wood", 10, ws[0]);
            if (newWood is null) return;
            foreach (var _wood in newWood)
            {
                wood.Add(_wood);
            }
            WoodCount.Content = $"Wood: {wood.Count}";
            Resources.Content = $"Resources : {wood.Count + steel.Count + coal.Count}";
        }

        private void btnRequestSteel_Click(object sender, RoutedEventArgs e)
        {
            if(ws.Count == 0)
            {
                return;
            }
            List<Resource> newSteel = RequestResourceChain("steel", 5, ws[0]);
            if (newSteel is null) return;
            foreach (var _steel in newSteel)
            {
                steel.Add(_steel);
            }
            SteelCount.Content = $"Steel: {steel.Count}";
            Resources.Content = $"Resources : {wood.Count + steel.Count + coal.Count}";
        }
        private void btnRequestCoal_Click(object sender, RoutedEventArgs e)
        {
            if (ws.Count == 0)
            {
                return;
            }
            List<Resource> newCoal = RequestResourceChain("coal", 6, ws[0]);
            if (newCoal is null) return;
            foreach(var _coal in newCoal)
            {
                coal.Add(_coal);
            }
            CoalCount.Content = $"Coal: {coal.Count}";
            Resources.Content = $"Resources : {wood.Count + steel.Count + coal.Count}";
        }
        private void SpawnDummies(IZombieWave wave)
        {
            List<string> imagePath = new List<string>();
            if (wave is FirstZombieWave)
            {
                imagePath.Add(
                    "pack://application:,,,/ZombieCityGame;component/images/zombie-def-lvl-1.png");
            }
            else if (wave is SecondZombieWave)
            {
                imagePath.Add(
                    "pack://application:,,,/ZombieCityGame;component/images/zombie-def-lvl-2.png");
            }
            foreach (var dummy in wave.Dummies)
            {
                ZombieDummyUI zombie = new ZombieDummyUI(dummy);
                zombie.SetImage(imagePath[(int)ZombieType.Dummy]);
                GameScreen.Children.Add(zombie.ZombieImage);
                zombie.SetPosition(GenaratePosition(GameScreen, CityArea.ActualWidth, zombie.ZombieImage.ActualHeight / 2));
                zombie.Spawn();
                ZombieDummies.Add(zombie);
            }
        }
        void SpawnAttackers(IZombieWave wave)
        {
            List<string> imagePath = new List<string>();
            if (wave is FirstZombieWave)
            {
              imagePath.Add(
                    "pack://application:,,,/ZombieCityGame;component/images/zombie-attacker-lvl-1.png");
            }
            else if (wave is SecondZombieWave)
            {
                imagePath.Add(
                    "pack://application:,,,/ZombieCityGame;component/images/zombie-attacker-lvl-2.png");
                imagePath.Add(
                    "pack://application:,,,/ZombieCityGame;component/images/zombie-night-hunter.png");
            }
            foreach (var attacker in wave.Attackers)
            {

                ZombieAttackerUI zombie = new ZombieAttackerUI(attacker);
                zombie.SetImage(imagePath[(int)AttackerType.Attacker]);
                if (attacker is ZombieAdvancedAttacker)
                {
                    zombie.SetImage(imagePath[(int)AttackerType.AdvancedAttacker]);
                }
                GameScreen.Children.Add(zombie.ZombieImage);
                zombie.SetPosition(GenaratePosition(GameScreen, CityArea.ActualWidth + 110, zombie.ZombieImage.ActualHeight / 2));
                zombie.Spawn();
                ZombieAttackers.Add(zombie);
            }
        }
        private void StartWave(List<ZombieDummyUI> dummies, List<ZombieAttackerUI> attackers)
        {
            foreach (var zombie in dummies)
            {
                zombie.Move();
            }
            foreach (var zombie in attackers)
            {
                zombie.Move();
                zombie.Attack();
                zombie.Update();
            }
        }
        private void GameTick(object? sender, EventArgs e)
        {
            if (isNight)
            {
                gameFacade.UpdateLevel(waveCount);
                isNight = false;
                StartWave(ZombieDummies, ZombieAttackers);

                Wave.Content = $"Wave : {waveCount}";
                isStarted = true;
            }
            if(isStarted)
            {
                StartWave(ZombieDummies, ZombieAttackers);
                return;
            }
        }

    }
}