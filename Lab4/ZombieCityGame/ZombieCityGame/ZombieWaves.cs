﻿using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace ZombieCityGame
{
    public interface IZombie
    { 
        double Speed { get; set; }
        double HP { get; set; }
        void Move();
    }
    public interface IZombieDummy : IZombie
    {
        void Stand();
    }
    public interface IZombieAttacker : IZombie
    {
        void Attack();
        void TakeCover();
        ZombieState State { get; set; }
        double Damage { get; set; }
    }
    public class ZombieDummy : IZombieDummy
    {
        public double Speed { get;  set; }
        public double HP { get;  set; }
        public ZombieDummy()
        {
            HP  = 100;
        }
        public void Move()
        {
            HP -= 2;
            Speed = 0.000004;
        }
        public void Stand()
        {
            Speed = 0;
            if(HP < 100)
            {
                HP += 4;
            }
        }
    }
    public class ZombieAttacker : IZombieAttacker
    { 
        public ZombieAttacker()
        {
            HP = 100;
            Speed = 0;
            Damage = 10;
            //state = new AggresiveAttacker(this);
        }
        public double Speed { get; set; }
        public double HP { get; set; }

        public double Damage { get; set; }

        private ZombieState state;
        public ZombieState State
        {
            get { return state; }
            set { state = value; }
        }
        public void Attack()
        {
            Damage += 1.5;
            HP -= 10;
        }

        public void Move()
        {
            HP -= 4;
            Speed = 0.000001;
        }
        public void TakeCover()
        {
            if(Damage < 15)
            {
                Damage += 1.0;
            }
            if(HP < 100)
            {
                HP += 5;
            }
            Speed = 0;
        }
    }
    class ZombieAdvancedAttacker : IZombieAttacker
    {
        public double Speed { get; set; }
        public double HP { get; set; }
        private ZombieState state;
        public ZombieState State
        {
            get { return state; }
            set { state = value; }
        }
        public double Damage { get; set; }
        public void Attack()
        {
            Damage += 1.5;
            HP -= 10;
        }

        public void Move()
        {
            HP -= 1;
            Speed = 0.0000001;
        }
        public void TakeCover()
        {
            Damage += 1.5;
            HP += 5;
            Speed = 0;
        }
        public void Heal()
        {
            Speed = 0;
            HP += 1.0;
        }
        public ZombieAdvancedAttacker()
        {
            HP = 120;
            Speed = 0;
            Damage = 12;
        }
    }

    
    public class NightHunterZombie : Decorator
    {
        public NightHunterZombie(ZombieAttacker zombie) : base(zombie)
        {

        }
        public void Attack()
        {
            base.Attack();
        }
        public void TakeCover()
        {
            base.TakeCover();
        }
    }

    namespace AbstractFactory
    {
        public interface IZombieWave
        {
            List<IZombieDummy> Dummies { get; set; }
            List<IZombieAttacker> Attackers { get; set; }
            int WaveSize {  get; set; }
            List<IZombieDummy> CreateDummies();
            List<IZombieAttacker> CreateAttackers();
            IZombieDummy CreateDummy();
            IZombieAttacker CreateAttacker();
            double Attack();
            void ReceiveDamage(double damage);
        }

        class FirstZombieWave : IZombieWave
        {
            private int waveSize;
            public int WaveSize { get => waveSize; set => waveSize = value; }
            public List<IZombieDummy> Dummies { get; set; }
            public List<IZombieAttacker> Attackers { get; set; }
            public IZombieDummy CreateDummy()
            {
                return new ZombieDummy();
            }
            public FirstZombieWave(int waveSize)
            {
                this.waveSize = waveSize;
            }
            public IZombieAttacker CreateAttacker()
            {
                ZombieAttacker zombieAttacker = new ZombieAttacker();
                zombieAttacker.State = new AggresiveAttacker(zombieAttacker);
                return zombieAttacker;
            }
            public List<IZombieDummy> CreateDummies()
            {
                Dummies = new List<IZombieDummy>();
                for (int i = 0; i < waveSize; i++)
                {
                    var dummy = CreateDummy();

                    Dummies.Add(dummy);
                }
                return Dummies;
            }
            public List<IZombieAttacker> CreateAttackers()
            {
                Attackers = new List<IZombieAttacker>();
                for (int i = 0; i < waveSize; i++)
                {
                    var attacker = CreateAttacker();

                    Attackers.Add(attacker);
                }
                return Attackers;
            }

            public double Attack()
            {
                return Attackers.Sum((attacker) => attacker.Damage);
            }

            public void ReceiveDamage(double damage)
            {
                damage /= (Dummies.Count + Attackers.Count);
                foreach (var dummies in Dummies)
                {
                    if(dummies.HP < damage)
                    {
                        dummies.HP = 0;
                    } 
                    else
                    {
                        dummies.HP -= damage;
                    }

                }
                foreach (var attacker in Attackers)
                {
                    if (attacker.HP < damage)
                    {
                        attacker.HP = 0;
                    }
                    else
                    {
                        attacker.HP -= damage;
                    }
                }
            }
        }
        class SecondZombieWave : IZombieWave
        {
            private int waveSize;
            public int WaveSize { get => waveSize; set => waveSize = value; }
            public List<IZombieDummy> Dummies { get; set; }
            public List<IZombieAttacker> Attackers { get; set; }
            public SecondZombieWave(int waveSize)
            {
                this.waveSize = waveSize;
            }
            public IZombieDummy CreateDummy()
            {
                return new ZombieDummy();
            }
            public IZombieAttacker CreateAttacker()
            {
                ZombieAttacker zombieAttacker = new ZombieAttacker();
                zombieAttacker.State = new AggresiveAttacker(zombieAttacker);
                return zombieAttacker;
            }
            public IZombieAttacker CreateAdvanced()
            {
                ZombieAdvancedAttacker zombieAttacker = new ZombieAdvancedAttacker();
                zombieAttacker.State = new AggresiveAttacker(zombieAttacker);
                return zombieAttacker;
            }
            public List<IZombieDummy> CreateDummies()
            {
                Dummies = new List<IZombieDummy>();
                for (int i = 0; i < waveSize; i++)
                {
                    var dummy = CreateDummy();

                    Dummies.Add(dummy);
                }
                return Dummies;
            }
            public List<IZombieAttacker> CreateAttackers()
            {
                Attackers = new List<IZombieAttacker>();
                for (int i = 0; i < waveSize/2; i++)
                {
                    var attacker = CreateAttacker();

                    Attackers.Add(attacker);
                }
                //AdvanceAttackers;
                for (int i = 0; i < waveSize / 2; i++)
                {
                    var attacker = CreateAdvanced();

                    Attackers.Add(attacker);
                }
                return Attackers;
            }
            public double Attack()
            {
                return Attackers.Sum((attacker) => attacker.Damage);
            }
            public void ReceiveDamage(double damage)
            {
                damage /= (Dummies.Count + Attackers.Count);
                foreach (var dummies in Dummies)
                {
                    if (dummies.HP < damage)
                    {
                        dummies.HP = 0;
                    }
                    else
                    {
                        dummies.HP -= damage;
                    }

                }
                foreach (var attacker in Attackers)
                {
                    if (attacker.HP < damage)
                    {
                        attacker.HP = 0;
                    }
                    else
                    {
                        attacker.HP -= damage;
                    }
                }
            }
        }
        public interface IWaveStrategy
        {
            void SetWave(IZombieWave wave);
            void SpawnZoimbies();
            List<IZombieAttacker> GetAttackers();
            List<IZombieDummy> GetDummies();
            IZombieWave GetWave();
        }
        public class LateNightStrategy : IWaveStrategy
        {
            private IZombieWave wave;
            public void SetWave(IZombieWave wave)
            {
                this.wave = wave;
            }
            public IZombieWave GetWave()
            {
                return wave;
            }
            public void SpawnZoimbies()
            {
                this.wave.CreateDummies();
                this.wave.CreateAttackers();
            }
            public List<IZombieAttacker> GetAttackers()
            {
                return wave.Attackers;
            }
            public List<IZombieDummy> GetDummies()
            {
                return wave.Dummies;
            }
        }
        public class DummiesStormStrategy : IWaveStrategy
        {
            private IZombieWave wave;
            public void SetWave(IZombieWave wave)
            {
                this.wave = wave;
            }
            public IZombieWave GetWave()
            {
                return wave;
            }
            public void SpawnZoimbies()
            {
                this.wave.CreateDummies();
            }
            public List<IZombieAttacker> GetAttackers()
            {
                return wave.Attackers;
            }
            public List<IZombieDummy> GetDummies()
            {
                return wave.Dummies;
            }
        }
        public class AttackersDawnStrategy : IWaveStrategy
        {
            private IZombieWave wave;
            public void SetWave(IZombieWave wave)
            {
                this.wave = wave;
            }
            public IZombieWave GetWave()
            {
                return wave;
            }
            public void SpawnZoimbies()
            {
                wave.Attackers = this.wave.CreateAttackers();
                for(int i = 0; i < wave.Attackers.Count/3; i++)
                {
                    this.wave.Attackers[i] = new PoisonZombie(this.wave.Attackers[i]);
                    this.wave.Attackers[i].State = new AggresiveAttacker(wave.Attackers[i]);
                }
            }
            public List<IZombieAttacker> GetAttackers()
            {
                return wave.Attackers;
            }
            public List<IZombieDummy> GetDummies()
            {
                return wave.Dummies;
            }
        }
        public class WaveContext
        {
            IWaveStrategy strategy;
            public WaveContext(IWaveStrategy strategy)
            {
                this.strategy = strategy;
            }
            public void SetStrategy(IWaveStrategy strategy)
            { 
                this.strategy = strategy; 
            }
            public void Spawn()
            {
                this.strategy.SpawnZoimbies();
            }
            public IZombieWave GetZombieWave()
            {
                return strategy.GetWave();
            }
            public List<IZombieAttacker> Attackers { get => this.strategy.GetAttackers(); }
            public List<IZombieDummy> Dummies { get => this.strategy.GetDummies(); }
        }
    }
}
