﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ZombieCityGame.Builder;

namespace ZombieCityGame
{
    public struct Position
    {
        public double x { get; set; }
        public double y { get; set; }
    }
    public class HP
    {
        public double Value { get; set; }
        public Position Position { get; set; }
    }
    
    
    namespace Singleton
    {
        //Singleton
        public sealed class City
        {
            private City() { }
            private static City? instance;
            private static object syncObj = new object();
            private Building townHall;
            public static City GetInstance()
            {
                if (instance == null)
                {
                    lock (syncObj)
                    {
                        if (instance != null)
                        {
                            return instance;
                        }
                        instance = new City();
                        return instance;
                    }
                }
                return instance;
            }
            public Building TownHall { 
                get => townHall;
                set => townHall = value;
            }
            
            private event EventHandler<GameOverEventArgs>? GameOver;

            private void OnGameOver(string message)
            {
                GameOver?.Invoke(this, new GameOverEventArgs(message));
            }  
            public void EndGame(string message)
            {
                OnGameOver(message);
            }
        }
        public class GameOverEventArgs: EventArgs
        {
            public string Message { get; }

            public GameOverEventArgs(string message)
            {
                Message = message;
            }
        }
    }
}

