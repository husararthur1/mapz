﻿//Task #1
using Microsoft.VisualBasic;
using System.Diagnostics;

//namespace Task1
//{
//    interface IWeapon
//    {
//        void Shoot();
//    }
//    abstract class Rifles: IWeapon
//    {
//        public abstract void Shoot();
//    }

//    class AssaultRifle : Rifles
//    {
//        public override void Shoot()
//        {
//            Console.WriteLine("Fire");
//        }
//    }
//}
////Task #2
//namespace Task2
//{
//    interface IBallon
//    {
//         void Fly();
//    }
//    class Basket
//    {
//        string Material { get; set; }
//    }
//    public class AirBalloon: IBallon
//    {
//        private Basket basket;
//        internal float armorPoints;
//        internal protected int passangersCount; 
//        private protected int basketReleaseCounter;
//        protected double gassAmount;
//        public string naming;
//        public void Fly()
//        {
//            this.gassAmount -= 0.2;
//        }
//    }
//    class Pilot : AirBalloon
//    {
//        void changeBasket()
//        {
//            armorPoints += 10;
//            passangersCount++;
//            basketReleaseCounter--;
//            //basket.Material = "Wood";
//            gassAmount = 10;
//            naming = "AB-200";
//        }
//        void Fly()
//        {

//        }
//    }
//    class Programm
//    {
//        static void Main(string[] args)
//        {
//            AirBalloon balloon = new AirBalloon();
//            //balloon.basket = new Basket();
//            //balloon.gassAmount = 10;
//            balloon.naming = "HAB-2000";
//        }
//    }
//}
////Task #3
//namespace Task3
//{
//    interface IDefend
//    {
//        void IncreaseDefenceLevel();
//    }
//    public class Building: IDefend
//    {
//        double defenceLevel;
//        class Room
//        {
//            int roomNumber; 
//        }
//        public void IncreaseDefenceLevel()
//        {

//        }
//    }
//    struct Tree
//    {
//        double age;
//        void Grow()
//        {
//            age += 1.2;
//        }
//        void CutDown ()
//        {
//            age = 0;
//        }
//    }

//    //class Program
//    //{
//    //    static void Main(string[] args)
//    //    {
//    //        Building building = new Building();
//    //        Tree tree = new Tree();
//    //        building.IncreaseDefenceLevel();
//    //        building.defenceLevel = 10.1;
//    //        tree.age = 12;
//    //        tree.Grow();
//    //        Building.Room room404 = new Building.Room();
//    //    }
//    //}
//}
////Task #4
//namespace Task4
//{
//    class MainTower
//    {
//        protected class Gates
//        {
//            double HP { get; set; }
//            public void Repair(double materials)
//            {
//                HP += materials;
//            }
//        }
//        Gates gates;
//        public MainTower()
//        {
//            gates = new Gates();
//        }
//        public void CallRepair(double materials)
//        {
//            gates.Repair(materials);
//        }
//    }

//    //class Program
//    //{
//    //    static void Main(string[] args)
//    //    {
//    //        MainTower cityHall = new MainTower();
//    //        MainTower.Gates mainGates = new MainTower.Gates();
//    //        mainGates.Repair(10);
//    //        cityHall.CallRepair(10);
//    //    }
//    //}
//}
////Task #5
//namespace Task5
//{
//    public enum StructureType
//    {
//        Barricade = 0,
//        Watchtower,
//        Hospital,
//        Armory,
//        Farm,
//        PowerPlant,
//        CommunicationCenter
//    }
//    class Program
//    {
//        static void Main(string[] args) 
//        {
//            StructureType structure1 = StructureType.Barricade;
//            StructureType structure2 = StructureType.CommunicationCenter;

//            bool areBothBarricades = structure1 == StructureType.Barricade && structure2 == StructureType.Barricade;
//            Console.WriteLine("Are both structures barricades? " + areBothBarricades);
//            bool areBothNotWatchtowers = (structure1 | structure2) != (StructureType.Watchtower);
//            Console.WriteLine("Is there any watchtower? " + areBothNotWatchtowers);
//            bool isMilitary = (structure1 ^ StructureType.Armory) == 0 || (structure1 ^ StructureType.Watchtower) == 0;
//            Console.WriteLine("Is there a watchtower or an armory? " + isMilitary);
//            bool isCivilian = (structure2 & StructureType.Armory) != 0 || (structure2 & StructureType.Watchtower) != 0;
//            Console.WriteLine("Do we have a civilian object nearby? " + isCivilian);

//        }
//    }
//}
////Task #6
//namespace Task6
//{

//    class Trap
//    {

//    }
//    interface IZombie 
//    {

//        // Додатковий метод для зомбі
//        public void Moan()
//        {
//            Console.WriteLine("Braaains...");
//        }
//    }

//    interface IHunter
//    {

//        // Додатковий метод для зомбі
//        public Trap SetUpHumanTrap()
//        {
//            return new Trap();
//        }
//    }

//    class ZombieHunter : IZombie, IHunter
//    {
//        string Name { get; set; }
//        public ZombieHunter(string name)
//        {
//            Name = name;
//        }
//        public void Moan()
//        {
//            Console.WriteLine($"{Name} says : \"Braaains...\"");
//        }

//        public Trap SetUpHumanTrap()
//        {
//            this.Moan();
//            Console.WriteLine($"{Name} says : \"Setting up a traaaap...\"");
//            return new Trap();
//        }
//    }
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            // Створюємо об'єкт класу ZombieHunter
//            ZombieHunter bob = new ZombieHunter("Bob");

//            bob.Moan();
//            bob.SetUpHumanTrap();
//        }
//    }
//}
////Task #7
//namespace Task7
//{
//    class Item
//    {
//        public string ItemType { get; set; }
//        public int Id { get; set; }
//    }
//    class Creature
//    {
//        public string Name { get; set; }
//        public Creature()
//        {
//            Name = "No name";
//        }
//        public Creature(string name)
//        {
//            Name = name;
//        }
//        public void Interact(string person)
//        {
//            Console.WriteLine($"[{Name}]: Greatings, {person}! I have some news to share");
//        }
//    }

//    class Vendor : Creature
//    {

//        protected List<Item> items;
//        public Vendor() : base()
//        {
//            items = null;
//        }
//        public Vendor(string name, List<Item> newItems) : base(name)
//        {
//            this.items = newItems;
//        }


//        public void Interact(string person)
//        {
//            Console.WriteLine($"[{Name}]: Greatings, {person}! I have some plans to share");
//        }
//        public void Interact(string person, List<Item> itemsToExchange)
//        {
//            Console.WriteLine($"[{Name}]: Greatings, {person}! I have some items to exchange : {itemsToExchange}");
//        }
//        public void Greet(string bob)
//        {
//            base.Interact(bob);
//            this.Interact(bob);
//        }
//    }
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            Creature goblin = new Creature("Will");
//            Vendor smith = new Vendor("Steve", null);
//            smith.Greet("Bob");
//        }
//    }
//}
//}
//namespace Task8
//{
//    class WatchTower
//    {
//        private static int installedTowersCount;
//        private int towersUnderConstruction;
//        static int waterGaloons = 10;
//        int waterL = 2;
//        static WatchTower()
//        {
//            Console.WriteLine($"Static constructor {installedTowersCount}, {waterGaloons}");
//            installedTowersCount = 30;
//        }

//        // Динамічний конструктор
//        public WatchTower()
//        {
//            Console.WriteLine("Dynamic constructor");
//            towersUnderConstruction = 40;
//        }
//    }
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            WatchTower watchTower = new WatchTower();
//        }
//    }
//}
//    namespace Task9
//{
//    class Zombie
//    {
//        public string Name { get; set; }
//        public Zombie(string name) 
//        {
//            Name = name;
//        }
//    }
//    class ZombieSlayer
//    {
//        public void KillZombies(int killedZombies, ref int totalKilledZombies, ref Zombie bob)
//        {
//            totalKilledZombies += killedZombies;
//        }

//        public void CountRemainingZombies(int totalZombies, int killedZombies, out int remainingZombies)
//        {
//            remainingZombies = totalZombies - killedZombies;
//        }
//    }
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            //ZombieSlayer bob = new ZombieSlayer();
//            //int zombiesCount = 100;
//            //int record;
//            //int remainingWave;
//            //Zombie zombie = new Zombie("Bob");
//            //bob.KillZombies(zombiesCount, ref record, ref zombie);
//            //Console.WriteLine($"{zombie.Name}");

//            //bob.CountRemainingZombies(zombiesCount, record, out remainingWave);
//            //Console.WriteLine($"{record}, {remainingWave}");

//            //Task #10
//            //Boxing
//            int zombieCount = 100;
//            Object obj = zombieCount;

//            //Unboxing
//            Object obj2 = 99;
//            int newZombieWave = (int)obj2;

//        }
//    }
//}

//namespace Task11
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            //Implicit
//            int zombieSerialNumber = 8;
//            double zombieHP = zombieSerialNumber;
//            zombieHP += 1.1;

//            //Expilit
//            zombieSerialNumber = (int)zombieHP;

//            Console.WriteLine(zombieHP); 
//            Console.WriteLine(zombieSerialNumber);

//        }
//    }
//}
namespace Task12
{
    class Zombie : Object
    {
        public override string ToString()
        {
            return "Braiiiins";
        }
    }

    class Buildings
    {
        public int baricadesCount;
    }
    class Baricades
    {

    }
    class WatchTower
    {
        public double HP { get; set; }
    }
    class Armory
    {
        public double Supplies { get; set; }
    }
    class ComunicationCenter 
    {
        
    }
    struct Bush
    {

    }
    public struct Tree : ILeaf, ITree
    {
        private int Age {  get; set; }
        private List<ILeaf> leaves;
        public void Fall()
        {
            leaves.Remove(this);
        }
        public void Grow()
        {
            Age += 1;
        }
        public struct Leaf
        {
            private void PrivateStructMethodTest () 
            {
                //Stopwatch stopwatch = new Stopwatch();
                //stopwatch.Start();
                for (int i = 0; i < 10; i++)
                {
                    Bush bush = new Bush();
                }
                //stopwatch.Stop();
                //Console.WriteLine($"[Nested Private Method]: Totall ms: {stopwatch.ElapsedMilliseconds}");
            }

            public void NestedStructTest()
            {
                //Stopwatch stopwatch = new Stopwatch();
                //stopwatch.Start();
                this.PrivateStructMethodTest();
                //stopwatch.Stop();
                //Console.WriteLine($"[Nested Private Struct Method Call]: Totall ms: {stopwatch.ElapsedMilliseconds}");
            }
        }
        private void PrivateStructMethodTest()
        {
            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();
            for (int i = 0; i < 10; i++)
            {
                Bush bush = new Bush();
            }
            //stopwatch.Stop();
            //Console.WriteLine($"[Private Method]: Totall ms: {stopwatch.ElapsedMilliseconds}");
        }

        public void StructTest()
        {
            Console.WriteLine("\n\nStructures test\n\n");
            Stopwatch stopwatch = new Stopwatch();
            Leaf leaf = new Leaf();
            stopwatch.Start();
            for(int i = 0; i < 1000000000; i++)
            {
                leaf.NestedStructTest();    
            }
            stopwatch.Stop();
            Console.WriteLine($"[1.000.000.000 Nested Private Struct Method Calls]: Totall ms: {stopwatch.ElapsedMilliseconds}");

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 1000000000; i++)
            {
                this.PrivateStructMethodTest();
            }
            sw.Stop();
            Console.WriteLine($"[1.000.000.000 Private Struct Method Calls]: Totall ms: {sw.ElapsedMilliseconds}");
        }
    }
    class City
    {
        protected List<Buildings> totalBuildings;
        
        class CityHall
        {
            public List<Baricades> totalBaricades;
            public Armory Armory { get; set; }
            private void Defend()
            {
                Baricades baricade = new Baricades();
                totalBaricades.Add(baricade);
                Armory.Supplies -= 10;
            }
            protected string CallComunacationCenter(string msg = "requesting supplies for armory")
            {
                Console.WriteLine($"CC this is City Hall: {msg}. Roger");

                return "4.5.0";
            }

            private void PrivateMethodTest()
            {
                //Stopwatch stopwatch = new Stopwatch();
                //stopwatch.Start();
                for (int i = 0; i < 10; i++)
                {
                    Tree tree = new Tree();
                   
                }
                //stopwatch.Stop();
                //Console.WriteLine($"[Nested Private Method]: Totall ms: {stopwatch.ElapsedMilliseconds}");
            }

            protected void ProtectedMethodTest()
            {
                for (int i = 0; i < 10; i++)
                {
                    Tree tree = new Tree();

                }
            }
            public void NestedTest()
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                for(int i = 0;i < 1000000000; i++)
                {
                    this.PrivateMethodTest();
                }
                sw.Stop();
                Console.WriteLine($"[1.000.000.000 Nested Class private methods calls]: Totall ms: {sw.ElapsedMilliseconds}");

                Stopwatch sw2 = new Stopwatch();

                sw2.Start();
                for (int i = 0; i < 1000000000; i++)
                {
                    this.ProtectedMethodTest();
                }
                sw2.Stop();
                Console.WriteLine($"[1.000.000.000 Nested Class protected methods calls]: Totall ms: {sw2.ElapsedMilliseconds}");
            }
        }
        private void RepairBuildings(List<Buildings> newBuildings)
        {
            foreach (var building in newBuildings)
            {
                totalBuildings.Add(building);
            }
            Console.WriteLine($"Total building count {totalBuildings.Count}");
        }
        protected void CallForAirStrike()
        {
            Console.WriteLine("Requesting Air Strike");
        }

        private void PrivateMethodTest()
        {
            for (int i = 0; i < 10; i++)
            {
                Tree tree = new Tree();

            }
        }

        protected void ProtectedMethodTest()
        {
            for (int i = 0; i < 10; i++)
            {
                Tree tree = new Tree();

            }
        }
        public void Test()
        {
            CityHall cityHall = new CityHall();
            cityHall.NestedTest();

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 1000000000; ++i)
            {
                this.PrivateMethodTest();
            }
            sw.Stop();
            Console.WriteLine($"[1.000.000.000 Main Class private methods calls]: Totall ms: {sw.ElapsedMilliseconds}");


            Stopwatch sw2 = new Stopwatch();


            sw2.Start();
            for (int i = 0; i < 1000000000; ++i)
            {
                this.PrivateMethodTest();
            }
            sw2.Stop();
            Console.WriteLine($"[1.000.000.000 Main Class protected methods calls]: Totall ms: {sw2.ElapsedMilliseconds}");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            City city = new City();
            city.Test();
            Tree tree = new Tree();
            tree.StructTest();
        }
    }

    interface ILeaf
    {
        public void Fall();
    }

    interface ITree
    {
        public void Grow();
    }

    
}