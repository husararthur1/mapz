﻿using System.Data;
using System.Text;
using ZombieCity;

namespace Lab3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StringBuilder str = new StringBuilder();

            str.AppendLine(String.Format("{0, -28} {1, -16} {2, -16} {3, -16}", "Adress", "Defense", "Resident Name", "Resident Role"));

            foreach (var building in City.Buildings)
            {
                var resident = City.Residents.Find(r => r.Id == building.ResidentId);
                str.AppendLine(String.Format("{0, -28} {1, -16} {2, -16} {3, -16}", building.Adress, building.IsBaricated ? "Baricated" : "Not Baricated",
                    resident.Name.ToString(), resident.Role.ToString()
                    ));
            }
            Console.WriteLine(str);


            //
            var groupedBuildings = City.Buildings.GroupBy(building => building.IsBaricated);
            foreach (var building in groupedBuildings)
            {
                foreach(var baricated in building)
                {
                    //Console.WriteLine(baricated.Adress);
                }
            }
            var sortedBuildings = City.Buildings.Join(
                City.Residents, building => building.BuildingId, resident => resident.Id, (building, resident) => new { Building = building, Resident = resident })
                .Where(b => b.Resident.Id > 2).OrderBy(b => b.Building.BuildingId);
            int i = 0;
            string[] expectdAdresses = { "Market st., 3, Los Santos", "Groove st., 5, Los Santos" };

            foreach (var building in sortedBuildings)
            {
                Console.WriteLine(building.Building.Adress);
                Console.WriteLine(expectdAdresses[i]);
                i++;
            }

            MyComparer cmp = new();
            var residents = City.Residents;
            residents.Sort(cmp);
            string[] roles = { "Hunter", "Nurse", "Mechanic", "Mechanic", "Worker" };
            int j = 0;
            foreach (var res in residents)
            {
                //Console.WriteLine($"{res.Role}, {roles[j]}");
                j++;
            }
            var sortedList = City.Buildings.GroupBy(b => b.ResidentId)
                .OrderByDescending(g => g.Count())
                .SelectMany(g => g)
                .ToList();
            //Console.WriteLine("123");
            foreach(var res in sortedList)
            {
                Console.WriteLine(res.ResidentId);
            }
            var rolesGroup = City.Residents.GroupBy(r => r.Role);
            foreach (var role in rolesGroup)
            {
                //Console.WriteLine(role.Key, role.);
            }
            var rolesCount = City.Residents.Roles();
            foreach (var role in rolesCount)
            {
                Console.WriteLine($"Role: {role.Key} Count: {role.Value}");
            }
            Console.WriteLine(rolesCount["Mechanic"]);
            var cities = new List<city>();
            cities.Add(new city());
            cities.Add(new city());
            cities.Add(new city());
            var b = City.Buildings.GroupBy(b => b.ResidentId).ToList();


            var selectedCities = cities.Where(city => city.Buildings.Count() > 1)
                .Where(city => city.Residents.Any(resident => resident.Name == "Allan"));
            var SelectedCities = cities.Where(city => city.Buildings.Count() > 1).Where(city => city.Buildings.Join(city.Residents,
                building => building.ResidentId, resident => resident.Id, (building, resident) => new
                {
                    Building = building,
                    Resident = resident,
                }).Any(b => b.Resident.Name == "Allan"));
            Console.WriteLine($"{selectedCities.Count()}, {SelectedCities.Count()}"); 
        }
    }
}
