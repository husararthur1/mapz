using System.Collections.Generic;
using System.Data;
using ZombieCity;

namespace UnitTestsProject
{
    public class CityUnitTest
    {
        [Fact]
        public void TestToDictionary() 
        {
            var dictionary = City.Buildings.ToDict(
                building => building.BuildingId,
                building => building
            );
            Assert.IsType<Dictionary<int, Building>>( dictionary );
        }
        [Fact]
        public void TestToSortedList()
        {
            var list = City.Buildings.ToSortedList(building => building.BuildingId);
            var expected = 0;
            Assert.Equal(expected, list.First().Value.BuildingId);
        }
        [Fact]
        public void TestToQueue()
        {
            var buildingQueue = new Queue<Building>(City.Buildings);
            Assert.Equal(buildingQueue.Peek().Adress, "Groove st., 0, Los Santos");
        }
        [Fact]
        public void TestToStack()
        {
            var buildingStack = new Stack<Building>(City.Buildings);
            Assert.Equal(buildingStack.Pop().Adress, "Groove st., 5, Los Santos");
        }
        [Fact]
        public void TestToHashSet()
        {
            var buildings = new HashSet<Building>(City.Buildings);
            Assert.Equal(buildings.Count(), City.Buildings.Count);
        }
        [Fact]
        public void TestGroup()
        {
            var groupedBuildings = City.Buildings.GroupBy(building => building.IsBaricated);
            var expectedBuildings = City.Buildings.Select(building => building).Where(isbaricated => isbaricated.IsBaricated == true);
            Assert.Equal(groupedBuildings.First().Count(), expectedBuildings.Count());
        }
        [Fact]
        public void TestOrderBy()
        {
            var sortedBuildings = City.Buildings.OrderBy(building => building.ResidentId).ToList();
            string expectedAdress = "Market st., 4, Los Santos";
            Assert.Equal(sortedBuildings[1].Adress, expectedAdress);
        }
        [Fact]
        public void TestOrderByDescending()
        {
            var sortedBuildings = City.Buildings.OrderByDescending(building => building.ResidentId).ToList();
            string expectedAdress = "Market st., 3, Los Santos";
            Assert.Equal(sortedBuildings[0].Adress, expectedAdress);
        }
        [Fact]
        public void TestComplexOps()
        {
            var sortedBuildings = City.Buildings.Join(
                City.Residents, building => building.BuildingId, resident => resident.Id, 
                (building, resident) => new { Building = building, Resident = resident })
                .Where(b => b.Resident.Id > 2).OrderBy(b => b.Building.BuildingId);
            string[] expectdAdresses = { "Market st., 3, Los Santos", "Market st., 4, Los Santos" };
            int i = 0;
            foreach (var building in sortedBuildings)
            {
                Assert.Equal(building.Building.Adress, expectdAdresses[i]);
                i++;
            }
        }
        [Fact]
        public void TestSelect()
        {
            var residents = City.Residents.Select(r => r.Role).ToList();
            string roles = "Worker";
            Assert.Contains(roles, residents.First());
        }
        [Fact] 
        public void TestWhere()
        {
            var nonBaricated = City.Buildings.Where(b => b.IsBaricated == false).ToList();
            Assert.Equal(nonBaricated.Count, 2);
        }
        [Fact]
        public void TestListDict()
        {
            var list = new List<Resident>(City.Residents);
            var dict = list.ToDictionary(r => r.Name, r => r.Role);
            var listRes = list.Where(r => r.Id == 1).ToList();
            var dictRes = dict.Where(r => r.Value == "Mechanic").ToList();

            Assert.Equal(listRes.Count, 1);
            Assert.Equal(dictRes.Count, 2);
        }
        [Fact]
        public void TestExstension()
        {
            var rolesCount = City.Residents.Roles();
            Assert.Equal(rolesCount["Mechanic"], 2);
        }
        [Fact]
        public void TestAnonymus()
        {
            var obj = new
            {
                BuildingId = 7,
                Adress = "Groove st., 0, Los Santos",
                Role = "Worker",
                ResidentId = 9,
            };
            var building = new Building { BuildingId = obj.BuildingId, Adress = obj.Adress, 
                IsBaricated = false, ResidentId = obj.ResidentId };
            Assert.Equal(obj.Adress, building.Adress);
        }
        [Fact]
        public void TestComparer()
        {
            MyComparer cmp = new();
            var residents = City.Residents;
            residents.Sort(cmp);
            string[] roles = { "Hunter",  "Mechanic", "Mechanic", "Nurse",  "Worker" };
            int i = 0;
            foreach (var res in residents)
            {
                Assert.Equal(res.Role, roles[i]);
                i++;
            }
        }
        [Fact]
        public void TestToArray() 
        {
            var list = City.Residents.ToList();
            var arr = list.ToArray();
            Assert.Equal(list.Count, arr.Length);
        }
        [Fact]
        public void TestSorting()
        {
            var sortedList = City.Buildings.GroupBy(b => b.ResidentId)
                .OrderByDescending(g => g.Count())
                .SelectMany(g => g)
                .ToList();
            Assert.Equal(sortedList[0].IsBaricated, true);
        }
    }
}